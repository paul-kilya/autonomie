endi.tests.forms.admin package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.tests.forms.admin.sale

Module contents
---------------

.. automodule:: endi.tests.forms.admin
   :members:
   :undoc-members:
   :show-inheritance:
