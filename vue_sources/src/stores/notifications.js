import { defineStore } from 'pinia'
import { crudGetters, crudState, crudActions } from './modelStore.ts'
import http from '@/helpers/http'

function notificationActions(name, channelName) {
  return {
    _callAction: async function (notification, actionName) {
      console.log(
        `Calling ${actionName} on ${name} ${notification.id} from pinia store`
      )
      let url = this.apiUrl
      if (!url && this.collectionUrl) {
        url = this.collectionUrl + '/' + notification.id
      }
      url += '/' + actionName
      await http.put(url).then(() => this.loadChannel())
      return this.collection
    },
    markRead: async function (notification) {
      return this._callAction(notification, 'mark_read')
    },
    postPone: async function (notification) {
      return this._callAction(notification, 'postpone')
    },
    loadChannel: async function (num_items = 5) {
      console.log('Calling loadChannel')
      console.log(this.collectionUrl)
      console.log(num_items)
      return this.loadCollection(
        ['title', 'body'],
        [],
        { items_per_page: num_items, page: 0 },
        { channel: channelName }
      )
    },
    ...crudActions(name),
  }
}

export const useNotificationMessageStore = defineStore('notification-message', {
  state: crudState('notification-message', {
    collectionUrl: '/api/v1/notifications',
  }),
  getters: crudGetters('notification-message'),
  actions: notificationActions('notification-message', 'message'),
})
export const useNotificationHeaderMessageStore = defineStore(
  'notification-header_message',
  {
    state: crudState('notification-header_message', {
      collectionUrl: '/api/v1/notifications',
    }),
    getters: crudGetters('notification-header_message'),
    actions: notificationActions(
      'notification-header_message',
      'header_message'
    ),
  }
)
export const useNotificationAlertStore = defineStore('notification-alert', {
  state: crudState('notification-alert', {
    collectionUrl: '/api/v1/notifications',
  }),
  getters: crudGetters('notification-alert'),
  actions: notificationActions('notification-alert', 'alert'),
})
