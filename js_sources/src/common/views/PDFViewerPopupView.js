import PDFViewerView from "common/views/PDFViewerView";
import { getOpt } from '../../tools.js';
import BaseViewerPopupView from "./BaseViewerPopupView";


const PDFViewerPopupView = BaseViewerPopupView.extend({
    initialize: function (options) {
        this.pdfUrl = options.pdfUrl;
        this.popupTitle = getOpt(this, 'popupTitle');
        this.documentTitle = getOpt(this, 'documentTitle');
    },
    showPreview() {
        const view = new PDFViewerView({
            fileUrl: this.pdfUrl,
            title: this.documentTitle,
        })
        this.showChildView('pdfpreview', view);
    },
});
export default PDFViewerPopupView;
