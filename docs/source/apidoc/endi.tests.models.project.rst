endi.tests.models.project package
=================================

Submodules
----------

endi.tests.models.project.test\_project module
----------------------------------------------

.. automodule:: endi.tests.models.project.test_project
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.models.project
   :members:
   :undoc-members:
   :show-inheritance:
