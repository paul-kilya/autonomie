import getFormConfigStore from './formConfig'
import getModelStore from './modelStore.ts'

export const useProjectStore = getModelStore('project')
export const useProjectConfigStore = getFormConfigStore('project')
