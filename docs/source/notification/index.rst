Notifications
==============

.. image:: ./_static/process_notification.png
   :width: 100%
   :alt: Process d'envoi des notifications


enDI envoie des notifications via différents canaux (NotificationChannel) :

- e-mail
- message (cloche en haut à droite)
- header_message (bandeau en haut de page)
- alert (modale)


Les notifications peuvent être envoyées immédiatement ou programmées plus tard.

Envoi de notification
----------------------

L'envoi de notification se fait en construisant un objet `endi.utils.notification.abstract::AbstractNotification` et en appelant l'utilitaire `endi.utils.notification.notification::notify`

.. code-block:: python

    from endi.utils.nofitication import AbstractNotification, notify

    abstract = AbstractNotification(body="Corps de message", title="Titre", key="message:internal")
    notify(request, abstract, group_names=['manager', 'admin'])


Destinataires
.............

Les notifications peuvent être envoyées à :

- Des groupes
- Des user_ids
- Une enseigne
- Le référent d'un compte de gestion sociale

Programmation d'une notification
.................................

Une notification peut être programmée en spécifiant un **due_datetime**

.. code-block:: python

    from endi.utils.nofitication import AbstractNotification, notify
    import datetime

    abstract = AbstractNotification(body="Corps de message", title="Titre", key="message:internal", due_datetime=datetime.datetime.now() + datetime.timedelta(days=180))
    notify(request, abstract, group_names=['manager', 'admin'])

Un `endi.models.notification.notification::NotificationEvent` est alors créé.

Type de notification et Channel d'envoi
.........................................

Une notification est typée, les types sont créés dans le populate (d'autres types peuvent être créés par des plugins extérieurs).
Lors de l'envoi d'une notification on précise le type de notification via une clé.

Le système de notification choisit alors le canal d'envoi à utiliser.

Pour l'instant le Channel d'envoi par défaut du type de la notification est utilisé.

Il est possible de forcer le canal d'envoi

.. code-block:: python

    notify(request, abstract, group_names=['manager', 'admin'], force_channel='mail')

Les Channels existant

- mail
- message
- header_message
- alert

Ligne de commande
------------------

.. code-block:: command

    endi-admin config.ini notify --help
