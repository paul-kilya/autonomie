"""
Handle Supplier Document related events
"""
import logging


from endi_base.mail import (
    format_link,
)

from endi.utils.notification import AbstractNotification, notify


logger = logging.getLogger(__name__)


ORDER_SUBJECT_TMPL = "Votre devis interne {estimation} a été validé par \
{customer}"
ORDER_BODY_TMPL = """\
Bonjour {supplier},

L'enseigne {customer} a émis un bon de commande pour le devis {estimation} \
que vous avez émis.

Vous pouvez consulter votre devis ici :
{url}
"""


def get_title(event) -> str:
    """
    return the subject of the email
    """
    source_estimation = event.node.source_estimation
    return ORDER_SUBJECT_TMPL.format(
        estimation=source_estimation.name,
        customer=event.node.company.name,
    )


def get_body(event) -> str:
    """
    return the body of the email
    """
    source_estimation = event.node.source_estimation
    url = event.request.route_url("/estimations/{id}.html", id=source_estimation.id)
    url = format_link(event.get_settings(), url)
    return ORDER_BODY_TMPL.format(
        supplier=source_estimation.company.name,
        customer=event.node.company.name,
        url=url,
        estimation=source_estimation.name,
    )


def get_notification(event) -> AbstractNotification:
    return AbstractNotification(
        key="supplier_order:status:valid", title=get_title(event), body=get_body(event)
    )


def notify_supplier_order_status_changed(event):
    """Notify end users when internal supplier order status changed"""
    if not event.status == "valid":
        return

    notify(
        event.request,
        get_notification(event),
        company_id=event.node.company_id,
    )
