def includeme(config):
    config.include(".tasks")
    config.include(".expenses")
    config.include(".suppliers_orders")
    config.include(".suppliers_invoices")
