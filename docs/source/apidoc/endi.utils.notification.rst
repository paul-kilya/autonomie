endi.utils.notification package
===============================

Submodules
----------

endi.utils.notification.abstract module
---------------------------------------

.. automodule:: endi.utils.notification.abstract
   :members:
   :undoc-members:
   :show-inheritance:

endi.utils.notification.career\_path module
-------------------------------------------

.. automodule:: endi.utils.notification.career_path
   :members:
   :undoc-members:
   :show-inheritance:

endi.utils.notification.channels module
---------------------------------------

.. automodule:: endi.utils.notification.channels
   :members:
   :undoc-members:
   :show-inheritance:

endi.utils.notification.notification module
-------------------------------------------

.. automodule:: endi.utils.notification.notification
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.utils.notification
   :members:
   :undoc-members:
   :show-inheritance:
