def test_supplier_invoice_validation_sets_official_number(
    mk_supplier_invoice,
    pyramid_request,
    csrf_request_with_db_and_user,
):
    from endi.models.config import Config

    Config.set("supplierinvoice_number_template", "{SEQGLOBAL}")

    supplier_invoice_1 = mk_supplier_invoice()
    supplier_invoice_2 = mk_supplier_invoice()

    assert supplier_invoice_1.official_number is None
    assert supplier_invoice_2.official_number is None

    supplier_invoice_1.set_validation_status("valid", csrf_request_with_db_and_user)
    assert supplier_invoice_1.official_number == "1"

    supplier_invoice_2.set_validation_status("valid", csrf_request_with_db_and_user)
    assert supplier_invoice_2.official_number == "2"


def test_supplier_invoice_duplicate(
    dbsession,
    half_cae_supplier_invoice,
):
    si = half_cae_supplier_invoice
    si2 = si.duplicate()
    dbsession.add(si2)
    dbsession.flush()

    assert len(si2.lines) == 2
    assert si2.lines[0].id != si.lines[0]

    assert si2.lines[0].description == si.lines[0].description
    assert si2.lines[0].ht == si.lines[0].ht
    assert si2.lines[0].tva == si.lines[0].tva
    assert si2.lines[0].type_id == si.lines[0].type_id
    assert si2.lines[0].business_id == si.lines[0].business_id

    assert si2.cae_percentage == si.cae_percentage
    assert si2.payer == si.payer
