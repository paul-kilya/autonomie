import Mn from 'backbone.marionette';
import { formatAmount } from '../../math.js';

const BookMarkView = Mn.View.extend({
    tagName: 'tr',
    className: 'bookmark-line',
    template: require('./templates/BookMarkView.mustache'),
    ui: {
        delete_btn: '.delete',
        insert_btn: '.insert',
        clickable_cells: '.clickable-cell',
    },
    triggers: {
        'click @ui.delete_btn': 'bookmark:delete',
        'click @ui.insert_btn': 'bookmark:insert',
        'click @ui.clickable_cells': 'bookmark:insert',
    },
    templateContext(){
        var typelabel = this.model.getTypeLabel();
        return {
            ht: formatAmount(this.model.get('ht')),
            tva: formatAmount(this.model.get('tva')),
            typelabel: typelabel,
            insertAttrs:  (
                "title='Sélectionner ce favori et l’ajouter' " +
                "aria-label='Sélectionner ce favori et l’ajouter'"
            ),
        }
    }
});

const BookMarkCollectionView = Mn.CollectionView.extend({
	className: "modal_content_layout",
    template: require('./templates/BookMarkCollectionView.mustache'),
    childViewContainer: 'tbody',
    childView: BookMarkView,
    childViewTriggers: {
        'bookmark:delete': 'bookmark:delete',
        'bookmark:insert': 'bookmark:insert',
    }
});
export default BookMarkCollectionView;
