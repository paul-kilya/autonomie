Ancien code js
==================

NB : Méthode à bannir

Les fichiers sont dans static/js/ (excepté le répertoire build/ ).

Un bon nombre de librairies utilisées sont installées comme paquets python avec
la commande pip (voir les packages js.* dans requirements.txt).

2022-10 les composantes concernées :

- Interaction js dans des pages statiques
- Congés
- Retour des Jobs celery


Templates
-----------

Les templates sont buildés à partir de la commande ::

    make js
