/*
 * File Name :  ActionButtonsWidget.js
 * Action Widget is composed by :
 
 *  - an optionnal primary Button
 *  - an optionnal DropDown description
 */

import Mn from 'backbone.marionette';
import ButtonWidget from './ButtonWidget.js';
import DropDownWidget from './DropDownWidget.js';
import ActionButtonCollection from "../base/models/ActionButtonCollection";
import ButtonCollectionWidget from "./ButtonCollectionWidget";

const ActionButtonsWidget = Mn.View.extend({
    /*
     *
     * Renders Action buttons
     *
     * Can render:
     * A primary item composed of one or several button(s) presented separately
     * from the others
     * A dropdown with a list of buttons
     *
     * For the primary button(s)
     * :param obj primary: An ButtonModel for a single button or
     * an Array of objects for several button
     *
     * ex for several buttons :
     *  primary: [
                    {
                        label: "Voir / Modifier",
                        action: "edit",
                        icon: "pen"
                     },
                     {
                        label: "Dupliquer",
                        action: "duplicate",
                        icon: "copy"
                     },
                ]
     *
     *
     * 
     * For the dropdown
     * :param obj collection: A ButtonCollection
     * :param str dropdownLabel: The label for the optionnal dropdown
     * :param str icon: The icon used for the dropdown
     * :param bool showLabel: Should the label of the dropdown button be showed
     * :param str orientation: dropdown right/left orientation regarding the need
     *
     * Full example : 
     *     let primary = new ButtonModel({label: 'View', icon: 'pencil', action: 'view'});
     *      let dropdown = new ButtonCollection([
                new ButtonModel({label: Duplicate', icon: 'copy', action: 'duplicate'}),
                new ButtonModel({label: 'Delete', icon: 'trash-alt', action: 'delete'}),
            ]);
     *     let view = new ActionButtonsWidget(
                {primary: primary, dropDownLabel: 'Actions', collection: dropdown}
            );
     *     this.showChildView('actions-container', view);
     *
     * Triggers action:clicked with param 'action'
     */
    template: require('./templates/ActionButtonsWidget.mustache'),
    regions: {
        primary: {'el': '.primary', replaceElement: true},
        dropdown: {'el': '.dropdown', replaceElement: true},
    },
    // On forward l'évènement action:clicked
    childViewTriggers: {
        'action:clicked': 'action:clicked'
    },
    onRender(){
        if (this.getOption('primary')){
            if (Array.isArray(this.getOption('primary'))) {
                let collection = new ActionButtonCollection(
                    this.getOption('primary')
                );
                this.showChildView(
                    'primary',
                    new ButtonCollectionWidget({collection: collection})
                );
            } else {
                this.showChildView(
                    'primary',
                    new ButtonWidget({model: this.getOption('primary')})
                );
            }
        }
        if (this.collection){
            this.showChildView(
                'dropdown',
                new DropDownWidget(this.options)
            );
        }
    },
});
export default ActionButtonsWidget;
