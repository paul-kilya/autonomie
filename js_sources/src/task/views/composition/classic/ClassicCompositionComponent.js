/*
 * Module name : CompositionComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import LinesComponent from './lines/LinesComponent.js';
import HtBeforeDiscountsView from './discount/HtBeforeDiscountsView.js';
import DiscountComponent from './discount/DiscountComponent.js';
import ExpenseHtComponent from './ExpenseHtComponent.js';

const template = require('./templates/CompositionComponent.mustache');

/**
 * Main Composition View
 * 
 * Displays the classic task line edition mode
 * 
 */

const ClassicCompositionComponent = Mn.View.extend({
    template: template,
    regions: {
        tasklines: '#tasklines',
        ht_before_discounts: '.ht-before-discounts',
        discounts: '#discounts',
        expenses_ht: '#expenses_ht',
        link_container: '.link-container',
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {},
    initialize(options) {
        console.log(options);
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.section_options = options['section'];
        this.totalmodel = options['totalmodel'];
        this.edit = options['edit'];
    },
    onRender() {
        if (this.section_options.hasOwnProperty('lines')) {
            this.showLinesComponent();
        }
        if (this.section_options.hasOwnProperty('discounts')) {
            let view = new HtBeforeDiscountsView({
                model: this.totalmodel
            });
            this.showChildView('ht_before_discounts', view);
            this.showDiscountComponent();
        }
        if (this.section_options.hasOwnProperty('expenses_ht')) {
            this.showExpenseHtBlock();
        }
    },
    showLinesComponent() {
        var lineSection = this.section_options['lines']
        var model = this.facade.request('get:model', 'common');
        var collection = this.facade.request(
            'get:collection',
            'task_groups'
        );
        var view = new LinesComponent({
            collection: collection,
            edit: this.edit,
            model: model,
            section: lineSection
        });
        this.showChildView('tasklines', view);
    },
    showDiscountComponent: function () {
        var section = this.section_options['discounts'];
        var collection = this.facade.request(
            'get:collection',
            'discounts'
        );
        var view = new DiscountComponent({
            collection: collection,
            edit: this.edit,
            section: section,
        });
        this.showChildView('discounts', view);
    },
    showExpenseHtBlock: function () {
        var model = this.facade.request('get:model', 'expense_ht');
        var view = new ExpenseHtComponent({
            model: model
        });
        this.showChildView('expenses_ht', view);
    }
});
export default ClassicCompositionComponent