import Bb from 'backbone';

import NodeFileModel from './NodeFileModel.js';

const NodeFileCollection = Bb.Collection.extend({
    model: NodeFileModel,
    asSelectOptions(){
        return this.models.map(
            x => ({
                value: x.get('id'),
                label: x.get('label'),
            })
        );
    }
});
export default NodeFileCollection;
