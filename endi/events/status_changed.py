import logging

from endi.events.expense import notify_expense_status_changed
from endi.events.tasks import (
    on_status_changed_alert_related_business,
    notify_task_status_changed,
)
from endi.events.supplier import notify_supplier_order_status_changed


logger = logging.getLogger(__name__)


class StatusChanged:
    """
    Event fired when a document changes its status
    """

    def __init__(self, request, node, status, comment=None):
        self.request = request
        self.node = node
        self.comment = comment
        self.status = status
        self.node_type = node.type_

    def get_settings(self):
        return self.request.registry.settings


def notify_on_status_changed(event):
    """
    Dispatch the event, wrap it with a node specific wrapper and the send email
    from it
    """
    logger.info("+ StatusChanged : Mail")

    if event.node.type_ == "expensesheet":
        notify_expense_status_changed(event)

    elif event.node_type in (
        "invoice",
        "estimation",
        "internalinvoice",
        "internalestimation",
    ):
        notify_task_status_changed(event)

    elif event.node_type == "internalsupplier_order":
        notify_supplier_order_status_changed(event)

    else:
        logger.info(
            " - no mail implemented on {} status change".format(event.node.type_)
        )
        return


def alert_related(event):
    """
    Dispatch the event to alert some related objects
    """
    logger.info("+ StatusChanged : Alert")
    if event.node_type in (
        "invoice",
        "estimation",
        "internalinvoice",
        "internalestimation",
        "cancelinvoice",
    ):
        on_status_changed_alert_related_business(event)


def includeme(config):
    config.add_subscriber(notify_on_status_changed, StatusChanged)
    config.add_subscriber(alert_related, StatusChanged)
