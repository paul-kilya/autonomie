import pytest
from pyramid.response import Response


@pytest.fixture
def routes(mk_config, config):
    # FIMXE: could be replaced by dummy testing stuff or by actual route
    # definitions
    #
    routes = [
        "/invoices/{id}",
        "/admin/sales/accounting/invoice/config",
        "/companies/{id}",
        "customer",
    ]
    for route in routes:
        config.add_route(route, route)


@pytest.fixture
def valid_invoice(dbsession, full_invoice):
    full_invoice.status = "valid"
    full_invoice.official_number = "1324XX"
    full_invoice.financial_year = 2019
    dbsession.merge(full_invoice)
    dbsession.flush()
    return full_invoice


def test_sage_invoice_export_page_view_preview(
    app_config,
    get_csrf_request_with_db_and_user,
    valid_invoice,
    routes,
):
    # REF #3046 (regression)
    from endi.views.export.invoice import SageInvoiceExportPage

    post = {
        "__formid__": "invoice_number_form",
        "financial_year": "2019",
        "start": "1324XX",
        "end": "1324XX",
        "exported": "true",
        "preview": "preview",
    }

    view = SageInvoiceExportPage(get_csrf_request_with_db_and_user(post=post))
    # just checks no crash/500
    result = view()

    # We do not want to test validation here, but if we hit validation error
    # first, we are not realy testing the view full process…
    assert not result.get("check_messages", {}).get("errors", [])


def test_sage_single_invoice_export_page(
    app_config,
    get_csrf_request_with_db_and_user,
    valid_invoice,
    routes,
):
    # REF #3044 (regression)
    from endi.views.export.invoice import SageSingleInvoiceExportPage

    request = get_csrf_request_with_db_and_user(
        context=valid_invoice,
        params=dict(force=True),
    )
    view = SageSingleInvoiceExportPage(request)

    # just checks no crash/500
    result = view()

    # If we got a dict, might be validation error
    assert isinstance(result, Response)
