# flake8: noqa: E401
from endi.interfaces import INotificationChannel
from .channels import (
    AlertNotificationChannel,
    HeaderMessageNotificationChannel,
    MessageNotificationChannel,
    MailNotificationChannel,
)
from .notification import (
    notify,
    notify_now,
    publish_event,
    clean_notifications,
)
from .abstract import AbstractNotification


def includeme(config):
    config.register_service_factory(
        MessageNotificationChannel, iface=INotificationChannel
    )
    config.register_service_factory(
        MessageNotificationChannel, iface=INotificationChannel, name="message"
    )
    config.register_service_factory(
        AlertNotificationChannel, iface=INotificationChannel, name="alert"
    )
    config.register_service_factory(
        HeaderMessageNotificationChannel,
        iface=INotificationChannel,
        name="header_message",
    )
    config.register_service_factory(
        MailNotificationChannel, iface=INotificationChannel, name="mail"
    )
