<%inherit file="${context['main_template'].uri}" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <div role='group'></div>
    <div role='group'>
        <a class='btn' onclick="window.openPopup('${export_xls_url}');" href='javascript:void(0);' title="Export au format Excel (xls) dans une nouvelle fenêtre" aria-label="Export au format Excel (xls) dans une nouvelle fenêtre">
        	${api.icon('file-excel')}
            Excel
        </a>
        <a class='btn' onclick="window.openPopup('${export_ods_url}');" href='javascript:void(0);' title="Export au format Open Document (ods) dans une nouvelle fenêtre" aria-label="Export au format Open Document (ods) dans une nouvelle fenêtre">
			${api.icon('file-spreadsheet')}        
            ODS
        </a>
    </div>
</div>
</%block>

<%block name='content'>

<div class='text_block'>
    <h2>
    	Vos comptes de résultat pour l’année <strong>${selected_year}</strong>
        % if current_year != selected_year:
            <% url = request.route_path("/companies/{id}/accounting/income_statement_measure_grids", id=request.context.id) %>
            <small>( <a href="${url}">voir l'année courante</a> )</small>
        % endif
    </h2>
    <h3>
        % if not grid.is_void and grid.get_updated_at() is not None:
            Mis à jour le 
            <strong>${grid.get_updated_at().strftime("%d/%m/%y")}</strong>
        % endif
    </h3>
</div>
<div class='search_filters'>
    ${form|n}
</div>

<div>
    % if not grid.is_void:
        <div class='table_container'>
            <table class="compte_resultat">
                <thead>
                    <tr class="row_month">
                        <th scope="col" class="col_text" title="Indicateur"><span class="screen-reader-text">Indicateur</span></th>
                        % for i in range(1, 13):
                            <th scope="col" class="col_number" title="${api.month_name(i).capitalize()}" aria-label="${api.month_name(i).capitalize()}">${api.short_month_name(i).capitalize()}
                            </th>
                        % endfor
                        <th scope="col" class="col_number" title="Total annuel">Total<span class="screen-reader-text"> annuel</span></th>
                        <th scope="col" class="col_number" title="Pourcentage du Chiffre d’Affaires" aria-label="Pourcentage du Chiffre d’Affaires">% CA</th>
                    </tr>
                </thead>
                <tbody
                    % if not show_decimals:
                       class="hide-decimals"
                   % endif
                >
                    % for type_, contains_only_zeroes, row in grid.rows:
                        <tr
                        % if type_.is_total:
                            class='row_recap row_number'
                        % else:
                            class='row_number'
                        % endif
                        >
                            % if show_zero_rows or not contains_only_zeroes:
                                <th scope="row">${type_.label |n }</th>
                                % for cell in row:
                                    <td class='col_number'>${cell | n}</td>
                                % endfor
                            % endif
                        </tr>
                    % endfor
                </tbody>
            </table>
        </div>
    % else:
        <h4>Aucun compte de résultat n'est disponible</h4>
    % endif
    </div>
</div>
</%block>
