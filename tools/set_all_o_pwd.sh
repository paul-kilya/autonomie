#!/bin/bash
echo "Setting 'o' password for everyone"
if [ "$1" == '-f' ]
then
    echo "Forcing"
    MYSQLCMD='mysql -u root'
    DBNAME='endi'
elif [ "$1" == "docker" ]
then
    echo "Docker mode"
    MYSQLCMD='mysql -u root -P3308 -h 127.0.0.1 -pmariadb'
    DBNAME='endi'
else
    echo "Enter the mysql command line needed to have root access (default: 'mysql -u root')"
    read MYSQLCMD
    if [ "$MYSQLCMD" == '' ]
    then
        MYSQLCMD='mysql -u root'
    fi
    echo "Enter the database name (default : 'endi')"
    read DBNAME
    if [ "$DBNAME" == '' ]
    then
        DBNAME='endi'
    fi
fi

echo "update login set pwd_hash=MD5('o');" | ${MYSQLCMD} ${DBNAME}
