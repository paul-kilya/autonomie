Passer endi en mode Solo
=========================

Depuis une instance endi existante, il est possible de conserver uniquement les données de vente d'une enseigne.

.. code-block:: console

    endi-company-export development.ini company <id de l'object Company>


Il est ensuite possible de modifier l'interface afin qu'elle soit plus adaptée à l'usage dans un cadre hors CAE.


Dans le fichier .ini de l'application rajouter :

.. code-block:: console

    endi.modules =

    endi.includes =
            endi.plugins.solo

* La première clé de configuration endi.modules vient indiquer qu'on ne veut aucun des *modules optionnels* d'enDI.

Cependant selon les cas certains modules peuvent être nécessaires ou intéressants, il faut alors les spécifier manuellement. Voilà une suggestion de modules qui peuvent être inclus de base pour tous les utilisateurs solo :

.. code-block:: console

    endi.modules=
        endi.views.price_study
        endi.views.progress_invoicing
        endi.views.supply
        endi.views.third_party.supplier
        endi.views.export.supplier_invoice
        endi.views.export.supplier_payment


* La deuxième clé vient configurer un plugin 'solo', à inclure après coup, qui va essentiellement modifier l'organisation des menus.

cf aussi :doc:`decoupage`.
