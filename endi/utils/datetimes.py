import logging
import datetime


logger = logging.getLogger(__name__)


def parse_date(value, default=None, format_="%Y-%m-%d"):
    """
    Get a date object from a string

    :param str value: The string to parse
    :param str default: The default value to return
    :param str format_: The format of the str date
    :returns: a datetime.date object
    """
    try:
        result = datetime.datetime.strptime(value, format_).date()
    except ValueError as err:
        logger.debug("{} is not a date".format(value))
        if default is not None:
            result = default
        else:
            raise err
    return result


def parse_datetime(
    str_datetime: str, default=None, format_: str = "%Y-%m-%d %H:%M:%S"
) -> datetime.datetime:
    """
    Transform a date string to a date object

    :param str str_date: The date string
    :param tuple formats: List of date format to try when parsing
    :return: A datetime object
    :rtype: datetime.date
    """
    try:
        result = datetime.datetime.strptime(str_datetime, format_)
    except ValueError as err:
        logger.debug("{} is not a date".format(str_datetime))
        if default is not None:
            result = default
        else:
            raise err

    return result


def get_current_year():
    return datetime.date.today().year


def utcnow(delay=0):
    """
    Add Timezone info to the 'now' datetime object
    Usefull for delaying celery calls

    """
    n = datetime.datetime.utcnow()
    if delay:
        n += datetime.timedelta(seconds=delay)
    return n


def date_to_datetime(date: datetime.date) -> datetime.datetime:
    """Generate a datetime (0h00) from a date"""
    base_time = datetime.datetime.min.time()
    return datetime.datetime.combine(date, base_time)
