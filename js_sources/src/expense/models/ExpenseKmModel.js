import ExpenseBaseModel from './ExpenseBaseModel.js';
import { formatPaymentDate } from '../../date.js';
import Radio from 'backbone.radio';

const ExpenseKmModel = ExpenseBaseModel.extend({
    defaults: {
        type: 'km',
        category: null,
        ht: null,
        start: "",
        end: "",
        description: "",
        customer_id: null,
        project_id: null,
        business_id: null
    },
    initialize(options) {
        if ((options['altdate'] === undefined) && (options['date'] !== undefined)) {
            this.set('altdate', formatPaymentDate(options['date']));
        }
        this.config = Radio.channel('config');
    },
    validation: {
        type_id: {
            required: true,
            msg: "est requis"
        },
        date: {
            required: true,
            pattern: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
            msg: "est requise"
        },
        km: {
            required: true,
            pattern: "amount2",
        },
    },
    getIndice() {
        /*
         *  Return the indice used for compensation of km fees
         */
        let type = this.getType();
        if (type === undefined) {
            return 0;
        } else {
            return parseFloat(type.get('amount'));
        }
    },
    getHT() {
        return parseFloat(this.get('ht'));
    },
    getTva() {
        return 0;
    },
    total() {
        return this.getHT()
    },
    getKm() {
        return parseFloat(this.get('km'));
    },
});
export default ExpenseKmModel;
