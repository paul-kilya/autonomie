.. enDI documentation master file, created by
   sphinx-quickstart on Fri Jan 25 10:44:18 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

enDI
=========

Contents:

.. toctree::
    :maxdepth: 2

    components
    decoupage
    lexique
    vente/index.rst
    comptabilite/index.rst
    javascript/index.rst
    formulaire_javascript
    sqlalchemy_index
    views
    classes
    styleguide
    pyramid_traversal
    celery
    service
    tests
    endisap
    endisolo
    notification/index.rst
    apidoc/endi.rst
    apidoc/endi_celery.rst
    apidoc/endi_base.rst
    js_templates


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

