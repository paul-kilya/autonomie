import logging
from typing import Dict

from endi.views.sale_product.routes import (
    PRODUCT_API_ROUTE,
    CATALOG_ROUTE,
    CATALOG_API_ROUTE,
    CATEGORY_API_ROUTE,
)

from endi.resources import sale_product_resources
from endi.views import (
    BaseView,
    JsAppViewMixin,
)

logger = logging.getLogger(__name__)


class SaleProductView(BaseView, JsAppViewMixin):
    title = "Catalogue des produits"

    def context_url(self, _query: Dict[str, str] = {}):
        return self.request.route_path(
            PRODUCT_API_ROUTE, id=self.context.id, _query=_query
        )

    def category_url(self):
        return self.request.route_path(
            CATEGORY_API_ROUTE,
            id=self.context.id,
        )

    def catalog_tree_url(self):
        return self.request.route_path(
            CATALOG_API_ROUTE,
            id=self.context.id,
        )

    def more_js_app_options(self):
        return dict(
            catalog_tree_url=self.catalog_tree_url(),
            category_url=self.category_url(),
        )

    def __call__(self):
        sale_product_resources.need()
        return dict(title=self.title, urls=self.get_js_app_options())


def includeme(config):
    config.add_view(
        SaleProductView,
        route_name=CATALOG_ROUTE,
        permission="list.sale_products",
        renderer="/sale/products.mako",
        layout="opa",
    )
    config.add_company_menu(
        order=3,
        label="Catalogue produits",
        route_name=CATALOG_ROUTE,
        route_id_key="company_id",
        routes_prefixes=[
            "sale_categories",
            "sale_category",
            "sale_products_group",
            "sale_product_groups",
            "sale_training_group",
            "sale_training_groups",
        ],
    )
