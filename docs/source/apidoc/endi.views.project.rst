endi.views.project package
==========================

Submodules
----------

endi.views.project.business module
----------------------------------

.. automodule:: endi.views.project.business
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.controller module
------------------------------------

.. automodule:: endi.views.project.controller
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.estimation module
------------------------------------

.. automodule:: endi.views.project.estimation
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.files module
-------------------------------

.. automodule:: endi.views.project.files
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.invoice module
---------------------------------

.. automodule:: endi.views.project.invoice
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.layout module
--------------------------------

.. automodule:: endi.views.project.layout
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.lists module
-------------------------------

.. automodule:: endi.views.project.lists
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.phase module
-------------------------------

.. automodule:: endi.views.project.phase
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.project module
---------------------------------

.. automodule:: endi.views.project.project
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.rest\_api module
-----------------------------------

.. automodule:: endi.views.project.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.project.routes module
--------------------------------

.. automodule:: endi.views.project.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.project
   :members:
   :undoc-members:
   :show-inheritance:
