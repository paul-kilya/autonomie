import datetime
from unittest.mock import Mock
import io

from endi.export.sage import SageCsvWriter
from endi.models.config import Config


def test_sage_csv_writer(dbsession):
    class TestWriter(SageCsvWriter):
        headers = [
            {"name": "libelle", "label": "Libellé"},
            {"name": "date", "label": "Date"},
        ]

    def mk_test_csv_writer():
        request = Mock()
        request.config = Config
        w = TestWriter(request=request, context=None)
        w.set_datas(
            [
                {"libelle": "123456789", "date": datetime.date(2022, 1, 1)},
            ]
        )
        return w

    # default (no conf)
    buf = mk_test_csv_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'

    # zero
    Config.set("accounting_label_maxlength", "0")
    buf = mk_test_csv_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'

    # lower than size
    Config.set("accounting_label_maxlength", "4")
    buf = mk_test_csv_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"1234";"010122"'

    # bigger than size
    Config.set("accounting_label_maxlength", "11")
    buf = mk_test_csv_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'
