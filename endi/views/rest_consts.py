def country_rest_view(request):
    from endi.consts.insee_countries import COUNTRIES

    return COUNTRIES


def department_rest_view(request):
    from endi.consts.insee_departments import DEPARTMENTS

    return DEPARTMENTS


def street_type_rest_view(request):
    from endi.consts.street_types import TYPES

    return TYPES


def street_number_complements_rest_view(request):
    from endi.consts.street_number_complements import LETTERS

    return LETTERS


def includeme(config):
    for (label, view) in (
        ("countries", country_rest_view),
        ("street_types", street_type_rest_view),
        ("street_number_complements", street_number_complements_rest_view),
        ("departments", department_rest_view),
    ):
        route = f"/api/v1/consts/{label}"
        config.add_route(route, route)
        config.add_view(
            view,
            route_name=route,
            renderer="json",
        )
