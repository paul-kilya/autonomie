Export des écritures au format Sage
===================================

Les écritures sont exportées par défaut au format Sage dans des formats CSV écrit dans des fichiers .txt.

La configuration par défaut est endi/__init__.py.

Elle correspond à la configuration suivante dans le fichier .ini

.. code-block::

    endi.services.treasury_invoice_producer=endi.compute.sage.InvoiceExportProducer
    endi.services.treasury_internalinvoice_producer=endi.compute.sage.InternalInvoiceExportProducer
    endi.services.treasury_invoice_writer=endi.export.sage.SageInvoiceCsvWriter

    endi.services.treasury_payment_producer=endi.compute.sage.PaymentExportProducer
    endi.services.treasury_internalpayment_producer=endi.compute.sage.InternalPaymentExportProducer
    endi.services.treasury_payment_writer=endi.export.sage.SagePaymentCsvWriter

    endi.services.treasury_expense_producer=endi.compute.sage.ExpenseExportProducer
    endi.services.treasury_expense_writer=endi.export.sage.SageExpenseCsvWriter

    endi.services.treasury_expense_payment_producer=endi.compute.sage.ExpensePaymentExportProducer
    endi.services.treasury_expense_payment_writer=endi.export.sage.SageExpensePaymentCsvWriter

    endi.services.treasury_supplier_invoice_producer=endi.compute.sage.SupplierInvoiceExportProducer
    endi.services.treasury_internalsupplier_invoice_producer=endi.compute.sage.InternalSupplierInvoiceExportProducer
    endi.services.treasury_supplier_invoice_writer=endi.export.sage.SageSupplierInvoiceCsvWriter

    endi.services.treasury_supplier_payment_producer=endi.compute.sage.SupplierPaymentExportProducer
    endi.services.treasury_supplier_payment_user_producer=endi.compute.sage.SupplierUserPaymentExportProducer
    endi.services.treasury_internalsupplier_payment_producer=endi.compute.sage.InternalSupplierPaymentExportProducer
    endi.services.treasury_supplier_payment_writer=endi.export.sage.SageSupplierPaymentCsvWriter
